-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 12, 2022 at 05:58 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `picture` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `description`, `picture`, `is_deleted`) VALUES
(1, 'TEST1', 'TEST@@@', '1641954240_man-woman-wearing-medical-face-260nw-1688301544.png', 0),
(2, 'dfyj', 'sth', '1641919723_11709497_396978213827340_5471971946247644293_n.jpg', 0),
(3, 'aasddff', 'asg', '1641920560_12079461_427450947446733_6640232969932019881_n.jpg', 0),
(4, 'asg', 'asdg', '1641920622_1_emergency-phone-double-sided-metal-sign-sign-hd.png', 0),
(5, 'asgg', 'asdg', '1641920636_222222222222us-icon_1142-7401.png', 0),
(6, 'asgg', 'sg', '1641920645_140075050-illustration-of-a-man-wearing-a-black-mask.png', 0),
(7, 'asdg', 'asf', '1641920657_AMBULANCE.jpg', 0),
(8, 'afsh', 'asg', '1641920668_930125-coronavaccine2-1-1.jpg', 0),
(9, 'hgff', 'awresgf', '1641920685_do-good-typography-260nw-572464285.jpg', 0),
(10, 'htarwe', 'waergt', '1641920697_images.png', 0),
(11, 'TEST', 'TEST!@@@', '1641953857_man-woman-wearing-medical-face-260nw-1688301544.png', 0),
(12, 'Hridoy', 'This is TEST Hridoy', '1641954340_AMBULANCE.jpg', 0),
(13, 'sisipantnai', 'C4<#EHJ?$r(6sFcQ', '1641963302_download.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `picture` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `title`, `description`, `picture`, `is_deleted`) VALUES
(1, 1, 'D', 'Z', '1641925846_0be08989-002b-401e-a0c1-a2b19bedb6ee_corona world.jpg', 0),
(2, 3, 'fsda', 'adf', '1641926617_0be08989-002b-401e-a0c1-a2b19bedb6ee_corona world.jpg', 0),
(3, 1, 'asdgf', 'asdf', '1641926646_222222222222us-icon_1142-7401.png', 1),
(4, 10, 'TEST', 'TEST!', '1641926669_man-woman-wearing-medical-face-260nw-1688301544.png', 0),
(5, 8, 'TEST', 'TEST', '1641953481_man-woman-wearing-medical-face-260nw-1688301544.png', 0),
(6, 9, 'f', 'f', '1641955598_unnamed.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `crud_project` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `crud_project` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
