<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trash</title>
</head>
<body>

    <?php
        require_once '../../vendor/autoload.php';
        use Pondit\Product;
        $productObject = new Product;
        // echo '<pre>';
        $Products = $productObject->trash();
        // print_r($Products);
        // die();
    ?>


   <a href="index.php"> <button style="
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            background-color: #4CAF50;">Go TO List Page </button></a>

    <?php
    
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }

    ?>

    <table  border="1" cellspacing=5 cellpadding=5 style="width: 470px; margin:0 auto;">
        <thead>
            <tr>
                <!-- <td>SL#</td> -->
                <td>SL#</td>
                <td>Title</td>
                <td>Description</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
            <?php 
            $sl = 0;
            foreach($Products as $product){ ?>
            <tr>
                <td><?php echo ++$sl?></td>
                <td><?php echo $product['title']?></td>
                <td><?php echo $product['description']?></td>
                <td>
                  <a href="restore.php?id=<?php echo $product['id'] ?>"
                        onclick="return confirm('Are you sure want to restore?')">
                        <button style="
                            border: none;
                            color: white;
                            padding: 5px 18px;
                            text-align: center;
                            text-decoration: none;
                            display: inline-block;
                            font-size: 16px;
                            margin: 4px 2px;
                            cursor: pointer;
                            background-color:  #4CAF50;
                            border-radius: 18px;">Restore
                            </button>
                        </a>

                 <a onclick="return confirm('Are you sure want to delete permanently?')"
                        href="delete.php?id=<?php echo $product['id'] ?>">
                        <button style="
                            border: none;
                            color: white;
                            padding: 5px 18px;
                            text-align: center;
                            text-decoration: none;
                            display: inline-block;
                            font-size: 16px;
                            margin: 4px 2px;
                            cursor: pointer;
                            background-color: #ff0000;
                            border-radius: 18px;">Delete
                            </button>
                    </a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>