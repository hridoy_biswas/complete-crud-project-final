<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../layouts/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"/>
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Edit Category</title>
  </head>
<body>

    <?php
    require_once '../../vendor/autoload.php';
    use Pondit\Category;

    $categoryObject = new Category;
    $category = $categoryObject->show($_GET['id']);

    if(isset($_SESSION['errors'])){
        foreach($_SESSION['errors'] as $error){
            echo $error.'<br>';
        }
        unset($_SESSION['errors']);
    }  
    ?>
    <div class="wrapper">
      <div class="title">Edit Category</div>
      <form action="update.php" method="POST" enctype="multipart/form-data">
        <div class="field">
        <input type="hidden" name="categories_id" value="<?php echo $category['id'] ?>">
        </div>
        <div class="field">
        <input name="title" value="<?php echo $category['title'] ?>" required placeholder="Enter Title"><br>
        </div>
        <div class="field">
        <input name="description" value="<?php echo $category['description'] ?>" placeholder="Description"><br>
        </div>
        <div class="field">
        <input type="file" name="picture" accept="image/*"><br>
        </div>
        <div class="field">
          <input type="submit" value="Update">
        </div>
      </form>
    </div>
</body>
</html>