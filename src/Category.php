<?php
namespace Pondit;
use PDO;
use PDOException;
class Category 
{
    private $conn = '';
    public $title = '';
    public $description = '';
    public $picture = '';
    const PAGINATE_PER_PAGE = 3;
   
    public function __construct()
    {
        try {
            session_start();
            // echo "Hridoy";
            $this->conn = new PDO("mysql:host=localhost;dbname=crud_project","root", "1111");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function index()
    {
        $perPage = self::PAGINATE_PER_PAGE;
        if(isset($_GET['page'])){
            $pageNumber = $_GET['page'];
            $offset = ($pageNumber-1) * $perPage;
            $sql = "SELECT * FROM `categories` where is_deleted = 0 order by id asc limit $perPage OFFSET $offset"; 
        }else{
            $sql = "SELECT * FROM `categories` where is_deleted = 0 order by id asc limit $perPage";
        }
        $stmt = $this->conn->query($sql);

        $countCategorySql = "SELECT COUNT(*) as total_category FROM `categories`";
        $countCategoryStmt = $this->conn->query($countCategorySql);
        $categoryCount = $countCategoryStmt->fetchColumn();

        return [
            'categories' => $stmt->fetchAll(),
            'categories_count' => $categoryCount
        ];

    }
    public function setData(array $data = [])
    {
        $errors = [];

        $tempName = $_FILES['picture']['tmp_name'];
        $originalName = $_FILES['picture']['name'];
        $imageSize = $_FILES['picture']['size'];

        // image type validation
        $explodedArray = explode('.', $originalName);
        $uploadImageType = end($explodedArray);
        $availableImageTypes = ['png', 'jpg', 'jpeg', 'gif', 'JPG'];
        if(!in_array($uploadImageType, $availableImageTypes)){
            $errors[] = 'Invalid image type';
        }

        //image size validation
        if($imageSize > 1048576){
            $errors[] = 'Invalid image size';
        }

        if(array_key_exists('title', $data) && !empty($data['title'])){
            $this->title = $data['title'];
        }else{
            $errors[] = 'title required';
        }

        if(array_key_exists('description', $data) && !empty($data['description'])){
            $this->description = $data['description'];
        }else{
            $errors[] = 'description required';
        }

        if(count($errors)){
            $_SESSION['errors'] = $errors;
            header('location: '.$_SERVER['HTTP_REFERER']);
        }else{
            $imageName = time().'_'. $originalName;
            $this->picture =  $imageName;
            move_uploaded_file($tempName, '../../assets/images/'.$imageName);
            return $this;
        }

    }
    public function store()
    {
        try{
            $query ="INSERT INTO categories(title, description, picture) VALUES(:title , :description, :picture)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':title' => $this->title,
                ':description' => $this->description,
                ':picture' => $this->picture,

            ));
            $_SESSION['message'] = 'Successfully Created !';

            header('Location:index.php');
        }   catch (PDOException $e){
            echo $e->getMessage();
        }
       
    }
    public function show($id)
    {
        $sql = 'SELECT * FROM `categories` WHERE id='.$id;
        $stmt = $this->conn->query($sql);
        return $stmt->fetch();
    }
    public function update($id)
    {
        $query ="UPDATE categories SET title=:title, description=:description, picture=:picture where id = ".$id;
        $stmt = $this->conn->prepare($query);
        $stmt->execute(array(
            ':title' => $this->title,
            ':description' => $this->description,
            ':picture' => $this->picture
        ));
        $_SESSION['message'] = 'Successfully Updated !';
        header('Location:index.php');
    }
    public function destroy($id)
    {
        try{
            $query ="UPDATE categories SET is_deleted=:deleted where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':deleted' => 1
            ));
            $_SESSION['message'] = 'Successfully Deleted !';
            header('Location:index.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    public function trash()
    {
        $sql = 'SELECT * FROM `categories` where is_deleted = 1';
        $stmt = $this->conn->query($sql);
        return $stmt->fetchAll();
    }
    public function restore($id)
    {
        try{
            $query ="UPDATE categories SET is_deleted=:deleted where id = ".$id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':deleted' => 0
            ));

            $_SESSION['message'] = 'Successfully Restored !';
            header('Location:trash.php');
        } catch (PDOException $e){  
            echo $e->getMessage();
        }
    }
    public function delete($id)
    {
        try{
            $sql = 'SELECT * FROM `categories` WHERE id='.$id;
            $stmt = $this->conn->query($sql);
            $category = $stmt->fetch();

            $query ="delete from categories where id=".$id;
            $stmt = $this->conn->query($query);
            $stmt->execute();

            unlink("../../assets/images/".$category['picture']);
            $_SESSION['message'] = 'Successfully Deleted !';
            header('Location:index.php');
        } catch (PDOException $e){
            echo $e->getMessage();
        }
    }

}

